'''
Downloads the Nessus DEB file
'''
import shutil
import click
import requests

DOWNLOAD_URL_BASE = 'https://www.tenable.com/downloads/pages/60/downloads/{}/get_download_file'

@click.command()
@click.option('--fileid', default='7795', help='File to get')
@click.argument('filename')
def main(fileid, filename):
    '''Download and save a Nessus package'''
    s = requests.Session()
    download_page = s.get('https://www.tenable.com/downloads/nessus').text
    for line in download_page.splitlines():
        if 'csrf-token' in line:
            csrf_token = line.split('"')[3]
    payload = {'method': 'get_download_file',
               'authenticity_token': csrf_token,
               'commit': 'I Agree',
               'i_agree_to_tenable_license_agreement': 'true',
               'utf8': '✓'}
    package_url = s.post(DOWNLOAD_URL_BASE.format(fileid), data=payload, allow_redirects=False).headers['Location']
    content = s.get(package_url, stream=True)
    with open(filename, 'wb') as newfile:
        shutil.copyfileobj(content.raw, newfile)

if __name__ == '__main__':
    main()
