FROM python:latest

WORKDIR /app
COPY requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt

COPY download_nessus.py /app/download_nessus.py
RUN python3 download_nessus.py nessus.deb

FROM debian:stretch
LABEL author="Nicklaus McClendon"
LABEL version="2.0.0"

WORKDIR /pkg/
COPY --from=0 /app/nessus.deb nessus.deb
RUN dpkg -i nessus.deb
RUN apt-get install -f

HEALTHCHECK CMD curl -k --fail https://127.0.0.1:8834 || exit 1
EXPOSE 8834

VOLUME ["/opt/nessus/"]
ENTRYPOINT ["/opt/nessus/sbin/nessus-service"]
CMD ["-q"]
