Docker Nessus
=============

[GitLab](https://gitlab.com/kulinacs/docker-nessus)

Deploys Nessus in a Docker Container. Requires a Nessus activation code. 

NOTE: Using this Docker Container accepts the Nessus ToS.

Docker Compose
--------------

`docker-compose` can be used for local deployment. The activation code is then linked to the volume, allowing you to redeploy the container with the same volume.
